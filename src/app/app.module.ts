import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AsideComponent } from './aside/aside.component';
import { ArticleComponent } from './article/article.component';
import { AboutComponent } from './about/about.component';

import { InformationModule } from './information/information.module'

const routingApplication: Routes = [
  { path: "about", component: AboutComponent },
  { path: "info", redirectTo:"/information", pathMatch: "full" },
  { path: "**", component: ArticleComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AsideComponent,
    ArticleComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routingApplication),
    InformationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
