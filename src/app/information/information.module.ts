import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherComponent } from './teacher/teacher.component';
import { StudentComponent } from './student/student.component';

import { RouterModule, Routes } from '@angular/router';

const routingInformation: Routes = [
  {path: "information/student", component: StudentComponent },
  {path: "information/teacher", component: TeacherComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routingInformation)
  ],
  declarations: [TeacherComponent, StudentComponent]
})
export class InformationModule { }
