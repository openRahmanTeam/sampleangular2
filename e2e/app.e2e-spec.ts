import { PaoBKPage } from './app.po';

describe('pao-bk App', () => {
  let page: PaoBKPage;

  beforeEach(() => {
    page = new PaoBKPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
